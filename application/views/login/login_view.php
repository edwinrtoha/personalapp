<h5 class="card-title text-center">Sign In</h5>
<form action="<?= base_url('login');?>" method="POST" class="form-signin">
    <div class="form-label-group">
        <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username"
            required autofocus>
        <label for="inputUsername">Username</label>
    </div>

    <div class="form-label-group">
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password"
            required>
        <label for="inputPassword">Password</label>
    </div>
    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign
        in</button>
</form>