<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function alert($behavior,$data){
    $ci = &get_instance();
    $allowed_data=array('type','title','text','footer');
    foreach($allowed_data as $allow){
        if(isset($data[$allow])){
            $temp[$allow]=$data[$allow];
        }
    }
    $data=$temp; unset($temp);
    switch ($behavior) {
        case 'set':
            $ci->session->set_flashdata('alert',$data);
            break;
        case 'tampil':
            $alert='swal({';
            foreach(array_keys($data) as $key){
                $alert.=$key.': \''.$data[$key].'\'';
                if($key!='text'){
                    $alert.=',';
                }
            }
            $alert.='});';
            return $alert;
            break;
        default:
            # code...
            break;
    }
}
?>