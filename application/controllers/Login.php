<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        if($this->akun_model->ceklogin()==1){
            redirect(base_url('dashboard'));
        }
    }
    public function index(){
        switch($this->input->method()){
            case 'post':
                $data=array(
                    'param'=>array(
                        'username'=>$this->input->post('username'),
                        'password'=>$this->input->post('password')
                    )
                );
                if($this->akun_model->login($data)==1){
                    $alert=array(
                        'type'=>'success',
                        'title'=>'Berhasil',
                        'text'=>'Selamat datang kembali'
                    );
                }
                else{
                    $alert=array(
                        'type'=>'danger',
                        'title'=>'Gagal',
                        'text'=>'Login gagal'
                    );
                }
                alert('set',$alert);
                redirect(base_url('dashboard'));
                break;
            default:
                $data=array('title'=>'LOGIN','content'=>'login/login_view');
                $this->load->view("login/layout/wrapper",$data);
                break;
        }
    }
}