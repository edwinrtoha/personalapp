<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
		parent ::__construct();
    }
	public function index(){
		$this->akun_model->logout();
		$alert=array(
			'type'=>'success',
			'title'=>'Berhasil',
			'text'=>'Anda berhasil logout'
		);
		alert('set',$alert);
		redirect(base_url('login'));
	}
}