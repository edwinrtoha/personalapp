<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        $this->load->model('akun_model');
    }
    public function login(){
        switch($this->input->method()){
            case 'post':
                $this->form_validation->set_rules('username','Username','required');
                $this->form_validation->set_rules('password','Password','required');
                if($this->form_validation->run()===TRUE){
                    $data=array(
                        'param'=>array(
                            'username'=>$this->input->post('username'),
                            'password'=>$this->input->post('password')
                        )
                    );
                    $response=NULL;
                    if($this->akun_model->login_api($data,$response)==1){
                        http_response_code(200);
                    }
                    else{
                        http_response_code(401);
                    }
                }
                else{
                    http_response_code(406);
                }
                echo json_encode($response,JSON_PRETTY_PRINT);
                break;
            default:
                http_response_code(405);
                break;
        }
    }
    public function validate_token(){
        switch($this->input->method()){
            case 'post':
                $this->form_validation->set_rules('token','Token','required');
                if($this->form_validation->run()===TRUE){
                    $token=$this->input->post('token');
                    $response=NULL;
                    if($this->akun_model->validate_token($token,$response)==1){
                        http_response_code(200);
                    }
                    else{
                        http_response_code(401);
                    }
                }
                else{
                    http_response_code(406);
                }
                echo json_encode($response,JSON_PRETTY_PRINT);
                break;
            default:
                http_response_code(405);
                break;
        }
    }
}