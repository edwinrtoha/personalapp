<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        if($this->akun_model->ceklogin()!=1){
            redirect(base_url('login'));
        }
    }
    public function index(){
        $data=array('title'=>'Dashboard - Tambah Sirkulasi','content'=>'dashboard/dashboard_view');
        $this->load->view("dashboard/layout/wrapper",$data);
    }
}