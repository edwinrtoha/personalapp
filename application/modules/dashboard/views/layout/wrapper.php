<?php require_once('head.php');?>

<body>
    <?php
        if($this->session->flashdata('alert')){
            echo '<script>'.alert('tampil',$this->session->flashdata('alert')).'</script>';
        }
    ?>
    <div class="container">
        <div class="jumbotron wrapper">
            <?php require_once('content.php');?>
        </div>
    </div>
    <script>
        var url = window.location.pathname;
        var activePage = url.substring(url.lastIndexOf('/')+1);
        $('.nav-link').each(function(){
            var currentPage = this.href.substring(this.href.lastIndexOf('/')+1);
            if (activePage == currentPage) {
                console.log(1);
                $(this).addClass('active');
            }
        });
    </script>
</body>

</html>