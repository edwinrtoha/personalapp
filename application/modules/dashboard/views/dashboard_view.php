<div id="home-menu">
    <div class="d-flex flex-wrap justify-content-around">
        <div class="col-md-2">
            <a href="<?= base_url('keuangan');?>" class="btn btn-success btn-block">
                <i class="fas fa-wallet"></i><br>
                <span>Keuangan</span>
            </a>
        </div>

        <div class="col-md-2">
            <a href="#" onclick="swal({type:'warning',title:'Belum Tersedia',text:'Fitur agenda belum tersedia'});" class="btn btn-warning btn-block">
                <i class="fas fa-calendar-alt"></i><br>
                <span>Agenda</span>
            </a>
        </div>
        
        <div class="col-md-2">
            <a href="<?= base_url('logout');?>" onclick="return confirm('Apakah anda yakin akan logout?')" class="btn btn-danger btn-block">
                <i class="fas fa-power-off"></i><br>
                <span>Logout</span>
            </a>
        </div>
    </div>
</div>