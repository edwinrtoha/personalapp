<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        if($this->akun_model->ceklogin()!=1){
            redirect(base_url('login'));
        }
        $this->load->model('keuangan_dompet_model');
    }
    public function tambahsirkulasi(){
        switch($this->input->method()){
            case 'post':
                $data=array('title'=>'Keuangan - Tambah Dompet','content'=>'keuangan/form/sirkulasi/form_tambah_sirkulasi_view');
                $data['dompet']=$this->keuangan_dompet_model->get();
                $this->load->view("dashboard/layout/form/wrapper",$data);
                break;
            default:
                http_response_code(405);
                break;
        }
    }
    public function tambahdompet(){
        switch($this->input->method()){
            case 'post':
                $data=array('title'=>'Keuangan - Tambah Dompet','content'=>'keuangan/form/dompet/form_tambah_dompet_view');
                $this->load->view("dashboard/layout/form/wrapper",$data);
                break;
            default:
                http_response_code(405);
                break;
        }
    }
    public function ubahdompet(){
        switch($this->input->method()){
            case 'post':
                $data=array('title'=>'Keuangan - ubah Dompet','content'=>'keuangan/form/dompet/form_ubah_dompet_view');
                $query=array(
                    'param'=>array(
                        'id'=>$this->input->get('id')
                    ),
                    'limit'=>1
                );
                $data['dompet']=$this->keuangan_dompet_model->get($query);
                $this->load->view("dashboard/layout/form/wrapper",$data);
                break;
            default:
                http_response_code(405);
                break;
        }
    }
}