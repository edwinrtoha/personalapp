<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        $this->load->model('keuangan_dompet_model');
        $this->load->model('keuangan_sirkulasi_model');
    }
    public function dompet($aksi=null){
        if(!isset($aksi)){
            switch($this->input->method()){
                case 'post':
                    $required=array('nama','saldo');
                    foreach($required as $wajib){
                        $this->form_validation->set_rules($wajib,ucwords(strtolower($wajib)),'required');
                    }
                    if($this->form_validation->run()===TRUE){
                        $data=array(
                            'nama'=>$this->input->post('nama'),
                            'saldo'=>$this->input->post('saldo')
                        );
                        if($this->keuangan_dompet_model->tambah($data)==1){
                            $response=$data;
                        }
                        else{
                            $response=array('error'=>'Dompet gagal di tambahkan');
                        }
                    }
                    else{
                        $response=array('error'=>'Kolom belum di isi dengan lengkap');
                    }
                    echo json_encode($response, JSON_PRETTY_PRINT);
                    break;
                default:
                    $param=$this->input->get();
                    if(isset($param['limit_data'])){
                        $limit=$param['limit_data']; unset($param['limit_data']);
                    }
                    $query=array(
                        'param'=>$param
                    );
                    if(isset($limit)){
                        $query['limit']=$limit; unset($limit);
                    }
                    $data=array();
                    if($query=$this->keuangan_dompet_model->get($query)){
                        if($query->num_rows()){
                            foreach($query->result() as $row){
                                $data[]=$row;
                            }
                        }
                    }
                    $response=$data;
    
                    echo json_encode($response, JSON_PRETTY_PRINT);
                    break;
            }
        }
        else{
            switch($aksi){
                case 'ubah':
                    switch($this->input->method()){
                        case 'post':
                            $required=array('nama');
                            foreach($required as $wajib){
                                $this->form_validation->set_rules($wajib, ucwords(strtolower($wajib)), 'required');
                            }
                            if($this->form_validation->run()===TRUE){
                                if($id=$this->input->get('id')){
                                    $data=array(
                                        'nama'=>$this->input->post('nama')
                                    );
                                    if($this->keuangan_dompet_model->ubah($id,$data)==1){
                                        $data=array('success'=>'Dompet berhasil di ubah');
                                    }
                                    else{
                                        $data=array('error'=>'Dompet gagal di ubah');
                                        http_response_code(400);
                                    }
                                }
                                else{
                                    $data=array('error'=>'Dompet gagal di ubah');
                                    http_response_code(400);
                                }
                            }
                            else{
                                $data=array('error'=>form_error());
                            }
                            $response=$data;
                            echo json_encode($response, JSON_PRETTY_PRINT);
                            break;
                        default:
                            http_response_code(405);
                            break;
                    }
                    break;
                case 'hapus':
                    $data=array();
                    if($id=$this->input->get('id')){
                        if($this->keuangan_dompet_model->hapus($id)==1){
                            $data=array('success'=>'Dompet berhasil di hapus');
                        }
                        else{
                            $data=array('error'=>'Dompet gagal di hapus');
                            http_response_code(400);
                        }
                    }
                    else{
                        $data=array('error'=>'Kolom belum di isi dengan lengkap');
                        http_response_code(400);
                    }
                    $response=$data;
                    echo json_encode($response, JSON_PRETTY_PRINT);
                    break;
                default:
                    http_response_code(404);
                    break;
            }
        }
    }
    public function sirkulasi($aksi=null){
        if(!isset($aksi)){
            switch($this->input->method()){
                case 'post':
                    $this->form_validation->set_rules('id_dompet','ID Dompet','required|integer|greater_than_equal_to[0]');
                    $this->form_validation->set_rules('keterangan','Keterangan','required');
                    $this->form_validation->set_rules('jenis','Jenis','required|in_list[keluar,masuk]');
                    $this->form_validation->set_rules('nominal','Nominal','required|numeric');
                    $this->form_validation->set_rules('timestamp_sirkulasi','Timestamp sirkulasi','required');
                    $data=array();
                    if($this->form_validation->run()===TRUE){
                        $data=array(
                            'id_dompet'=>$this->input->post('id_dompet'),
                            'keterangan'=>$this->input->post('keterangan'),
                            'jenis'=>$this->input->post('jenis'),
                            'nominal'=>$this->input->post('nominal'),
                            'timestamp_sirkulasi'=>$this->input->post('timestamp_sirkulasi')
                        );
                        if($this->keuangan_sirkulasi_model->tambah($data)==1){
                            $response=$data;
                        }
                        else{
                            $response=array('error'=>'Dompet gagal di tambahkan');
                        }
                    }
                    else{
                        $response=array('error'=>'Kolom belum di isi dengan lengkap');
                    }
                    echo json_encode($response, JSON_PRETTY_PRINT);
                    break;
                default:
                    $query=array(
                        'param'=>$this->input->get()
                    );
                    $data=array();
                    if($query=$this->keuangan_sirkulasi_model->get($query)){
                        if($query->num_rows()){
                            foreach($query->result() as $row){
                                $data[]=$row;
                            }
                        }
                    }
                    $response=$data;
    
                    echo json_encode($response, JSON_PRETTY_PRINT);
                    break;
            }
        }
        else{
            switch($aksi){
                case 'hapus':
                    $data=array();
                    if($id=$this->input->get('id')){
                        if($this->keuangan_sirkulasi_model->hapus($id)==1){
                            $data=array('success'=>'Sirkulasi berhasil di hapus');
                        }
                        else{
                            $data=array('error'=>'Sirkulasi gagal di hapus');
                        }
                    }
                    else{
                        $data=array('error'=>'Kolom belum di isi dengan lengkap');
                    }
                    $response=$data;
                    echo json_encode($response, JSON_PRETTY_PRINT);
                    break;
                default:
                    http_response_code(404);
                    break;
            }
        }
    }
}