<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        if($this->akun_model->ceklogin()!=1){
            redirect(base_url('login'));
        }
        $this->load->model('keuangan_sirkulasi_model');
    }
    public function index(){
        switch($this->input->method()){
            case 'post':
                $data=array(
                    'id_dompet'=>$this->input->post('id_dompet'),
                    'keterangan'=>$this->input->post('keterangan'),
                    'jenis'=>$this->input->post('jenis'),
                    'nominal'=>$this->input->post('nominal'),
                    'timestamp_sirkulasi'=>$this->input->post('timestamp_sirkulasi')
                );
                if($this->keuangan_sirkulasi_model->tambah($data)==1){
                    $alert=array(
                        'type'=>'success',
                        'title'=>'Berhasil',
                        'message'=>'Dompet berhasil di ubah'
                    );
                }
                else{
                    $alert=array(
                        'type'=>'danger',
                        'title'=>'Gagal',
                        'message'=>'Dompet gagal di ubah'
                    );
                }
                alert('set',$alert);
                redirect(base_url('keuangan'));
                break;
            default:
                $data=array('title'=>'Dashboard - Tambah Sirkulasi','content'=>'keuangan/sirkulasi/keuangan_sirkulasi_view');
                $data['sirkulasi']=$this->keuangan_sirkulasi_model->get();
                $this->load->view("dashboard/layout/wrapper",$data);
                break;
        }
    }
    public function hapus(){
        if($id=$this->input->get('id')){
            if($this->keuangan_sirkulasi_model->hapus($id)==1){
                $alert=array(
                    'type'=>'success',
                    'title'=>'Berhasil',
                    'text'=>'Sirkulasi berhasil di hapus'
                );
            }
            else{
                $alert=array(
                    'type'=>'error',
                    'title'=>'Gagal',
                    'text'=>'Sirkulasi gagal di hapus'
                );
            }
        }
        else{
            $alert=array(
                'type'=>'error',
                'title'=>'Gagal',
                'text'=>'Sirkulasi gagal di hapus'
            );
        }
        alert('set',$alert);
        redirect(base_url('keuangan'));
    }
}