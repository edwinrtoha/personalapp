<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dompet extends CI_Controller {
	var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        if($this->akun_model->ceklogin()!=1){
            redirect(base_url('login'));
        }
        $this->load->model('keuangan_dompet_model');
    }
    public function index(){
        switch($this->input->method()){
            case 'post':
                $data=array(
                    'nama'=>$this->input->post('nama'),
                    'saldo'=>$this->input->post('saldo')
                );
                if($this->keuangan_dompet_model->tambah($data)==1){
                    $alert=array(
                        'type'=>'success',
                        'title'=>'Berhasil',
                        'message'=>'Dompet berhasil di tambahkan'
                    );
                }
                else{
                    $alert=array(
                        'type'=>'danger',
                        'title'=>'Gagal',
                        'message'=>'Dompet gagal di tambahkan'
                    );
                }
                alert('set',$alert);
                redirect(base_url('keuangan/dompet'));
                break;
            default:
                $data=array('title'=>'Dashboard - Tambah Dompet','content'=>'keuangan/dompet/keuangan_dompet_view');
                $data['dompet']=$this->keuangan_dompet_model->get();
                $this->load->view("dashboard/layout/wrapper",$data);
                break;
                http_response_code(405);
        }
    }
    public function ubah(){
        switch($this->input->method()){
            case 'post':
                if($id=$this->input->get('id')){
                    $data=array(
                        'nama'=>$this->input->post('nama')
                    );
                    if($this->keuangan_dompet_model->ubah($id,$data)==1){
                        $alert=array(
                            'type'=>'success',
                            'title'=>'Berhasil',
                            'message'=>'Dompet berhasil di ubah'
                        );
                    }
                    else{
                        $alert=array(
                            'type'=>'danger',
                            'title'=>'Gagal',
                            'message'=>'Dompet gagal di ubah'
                        );
                    }
                }
                else{
                    $alert=array(
                        'type'=>'danger',
                        'title'=>'Gagal',
                        'message'=>'Dompet gagal di ubah'
                    );
                }
                alert('set',$alert);
                redirect(base_url('keuangan/dompet'));
                break;
            default:
                http_response_code(405);
                break;
        }
    }
    public function hapus(){
        if($id=$this->input->get('id')){
            if($this->keuangan_dompet_model->hapus($id)==1){
                $alert=array(
                    'type'=>'success',
                    'title'=>'Berhasil',
                    'text'=>'Dompet berhasil di hapus'
                );
            }
            else{
                $alert=array(
                    'type'=>'error',
                    'title'=>'Gagal',
                    'text'=>'Dompet gagal di hapus'
                );
            }
        }
        else{
            $alert=array(
                'type'=>'error',
                'title'=>'Gagal',
                'text'=>'Dompet gagal di hapus'
            );
        }
        alert('set',$alert);
        redirect(base_url('keuangan/dompet'));
    }
}