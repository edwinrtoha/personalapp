<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Keuangan_sirkulasi_model extends CI_Model{
        var $CI = NULL;
        public function __construct() {
            $this->CI =& get_instance();
        }
        function get($data=''){
            if(!empty($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id','id_dompet','jenis');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }
    
                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }
            }

            $this->db->select('sirkulasi.*');
            $this->db->select('dompet.nama as nama_dompet');
            $this->db->join('dompet','dompet.id=sirkulasi.id_dompet','left');
            $query=$this->db->get('sirkulasi');

            return $query;
        }
        function tambah(&$data){
            // Mendaftarkan kolom yang di bolehkan
            $allowed=array('id_dompet','keterangan','jenis','nominal','timestamp_sirkulasi');
            // Filter array : hanya mengambil array dengan key sesuai dengan yang di izinkan
            foreach ($allowed as $allow) {
                $temp[$allow]=$data[$allow];
            }
            $data=$temp; unset($temp);

            if(is_array($data['timestamp_sirkulasi'])){
                $data['timestamp_sirkulasi']=implode(' ',$data['timestamp_sirkulasi']).':00';
            }

            if($data['jenis']=='keluar' AND $data['nominal']>0){
                $data['nominal']*=-1;
            }
            elseif($data['jenis']=='masuk' AND $data['nominal']<0){
                $data['nominal']*=-1;
            }
            
            // memulai transaksi database
            $this->db->trans_begin();
            foreach(array_keys($data) as $key){
                // mencegah serangan html injection
                $data[$key]=htmlspecialchars($data[$key]);
                // mencegah serangan sql injection
                $data[$key]=$this->db->escape_str($data[$key]);
            }
            // eksekusi query
            if($query=$this->db->insert("sirkulasi",$data)){
                $data=array_merge(array('id'=>$this->db->insert_id()),$data);
                if($this->db->trans_status()===true){
                    $this->db->trans_commit();
                    return 1;
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                $this->db->trans_rollback();
                return 0;
            }
        }
        function hapus($id){
            $this->db->trans_begin();
            $this->db->where('id',$this->db->escape_str($id));
            if($query=$this->db->delete("sirkulasi")){
                if($this->db->trans_status()===true){
                    if($this->db->affected_rows()>0){
                        $this->db->trans_commit();
                        return 1;
                    }
                    else{
                        $this->db->trans_rollback();
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                $this->db->trans_rollback();
                return 0;
            }
        }
    }
?>