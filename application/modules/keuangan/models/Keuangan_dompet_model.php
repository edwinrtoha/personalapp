<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Keuangan_dompet_model extends CI_Model{
        var $CI = NULL;
        public function __construct() {
            $this->CI =& get_instance();
        }
        function get($data=''){
            if(!empty($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id','nama');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }
    
                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }
            }

            $query=$this->db->get('dompet');

            return $query;
        }
        function tambah(&$data){
            // Mendaftarkan kolom yang di bolehkan
            $allowed=array('nama','saldo');
            // Filter array : hanya mengambil array dengan key sesuai dengan yang di izinkan
            foreach ($allowed as $allow) {
                $temp[$allow]=$data[$allow];
            }
            $data=$temp; unset($temp);
            
            // memulai transaksi database
            $this->db->trans_begin();
            foreach(array_keys($data) as $key){
                // mencegah serangan html injection
                $data[$key]=htmlspecialchars($data[$key]);
                // mencegah serangan sql injection
                $data[$key]=$this->db->escape_str($data[$key]);
            }
            // eksekusi query
            if($query=$this->db->insert("dompet",$data)){
                if($this->db->trans_status()===true){
                    $this->db->trans_commit();
                    return 1;
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                $this->db->trans_rollback();
                return 0;
            }
        }
        function ubah($id,&$data){
            $id=$this->db->escape_str($id);
            // Mendaftarkan kolom yang di bolehkan
            $allowed=array('nama');
            // Filter array : hanya mengambil array dengan key sesuai dengan yang di izinkan
            foreach ($allowed as $allow) {
                $temp[$allow]=$data[$allow];
            }
            $data=$temp; unset($temp);
            
            // memulai transaksi database
            $this->db->trans_begin();
            foreach(array_keys($data) as $key){
                // mencegah serangan html injection
                $data[$key]=htmlspecialchars($data[$key]);
                // mencegah serangan sql injection
                $data[$key]=$this->db->escape_str($data[$key]);
            }
            // eksekusi query
            $this->db->where('id',$id);
            if($query=$this->db->update("dompet",$data)){
                if($this->db->affected_rows()>0){
                    if($this->db->trans_status()===true){
                        $this->db->trans_commit();
                        return 1;
                    }
                    else{
                        $this->db->trans_rollback();
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                $this->db->trans_rollback();
                return 0;
            }
        }
        function hapus($id){
            $this->db->trans_begin();
            $this->db->where('id',$this->db->escape_str($id));
            if($query=$this->db->delete("dompet")){
                if($this->db->trans_status()===true){
                    if($this->db->affected_rows()>0){
                        $this->db->trans_commit();
                        return 1;
                    }
                    else{
                        $this->db->trans_rollback();
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                $this->db->trans_rollback();
                return 0;
            }
        }
    }
?>