<div id="menu">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard');?>"><i class="fas fa-arrow-left"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('keuangan');?>">Sirkulasi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('keuangan/dompet');?>">Dompet</a>
        </li>
    </ul>
</div>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Nominal</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php if($dompet->num_rows()):?>
                <?php foreach($dompet->result() as $row):?>
                    <tr>
                        <td><?= $row->nama;?></td>
                        <td>Rp<?php echo number_format($row->saldo, 0, ".", ".");?></td>
                        <td>
                            <div class="dropdown show">
                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button"
                                    id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    Aksi
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" class="btn btn-danger" href="<?= base_url('keuangan/dompet/hapus?id='.$row->id);?>">Hapus</a>
                                    <button class="dropdown-item ubahbtn" data-id="<?= $row->id;?>">Ubah</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php else:?>
                <tr>
                    <td colspan="3"><center>Tidak ada data</center></td>
                </tr>
            <?php endif;?>
        </tbody>
    </table>
</div>
<button id="tambahbtn" class="btn-float"><i class="fa fa-plus"></i></button>
<script>
    $('#tambahbtn').on('click', function (e) {
        $.ajax({
            url: '<?php echo base_url("keuangan/form/tambahdompet");?>',
            type: 'post',
            data: '',
            success: function (data) {
                swal({
                    title: 'Tambah Dompet',
                    html: data,
                    showConfirmButton: false
                });
            }
        });
    });
    $('.ubahbtn').on('click', function (e) {
        var id = $(this).attr("data-id");
        $.ajax({
            url: '<?php echo base_url("keuangan/form/ubahdompet?id=");?>'+id,
            type: 'post',
            data: '',
            success: function (data) {
                swal({
                    title: 'Ubah Dompet',
                    html: data,
                    showConfirmButton: false
                });
            }
        });
    });
</script>