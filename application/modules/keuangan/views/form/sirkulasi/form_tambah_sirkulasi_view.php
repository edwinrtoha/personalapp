<form action="<?= base_url('keuangan');?>" method="post">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal</label>
                <input type="date" name="timestamp_sirkulasi[]" value="<?= date('Y-m-d');?>" max="<?= date('Y-m-d');?>" class="form-control" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Waktu</label>
                <input type="time" name="timestamp_sirkulasi[]" min="00:00" value="00:00" class="form-control" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Dompet</label>
                <select name="id_dompet" class="form-control"<?= !$dompet->num_rows() ? ' disabled' : '';?>>
                    <?php if($dompet->num_rows()):?>
                        <option value="">- Pilih Dompet -</option>
                        <?php foreach($dompet->result() as $row_dompet):?>
                            <option value="<?= $row_dompet->id;?>"><?= $row_dompet->nama;?></option>
                        <?php endforeach;?>
                    <?php else:?>
                        <option value="">- Belum ada dompet -</option>
                    <?php endif;?>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Jenis</label>
                <select name="jenis" class="form-control" required>
                    <option value="">- Jenis -</option>
                    <option value="masuk">Masuk</option>
                    <option value="keluar">Keluar</option>
                </select>
            </div>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label>Nominal</label>
                <input type="number" name="nominal" value="0" min="0" class="form-control" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Keterangan</label>
                <textarea name="keterangan" cols="30" rows="4" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-success btn-lg">Simpan</button>
    </div>
</form>