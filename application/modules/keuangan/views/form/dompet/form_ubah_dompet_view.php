<?php if($dompet->num_rows()):?>
    <?php foreach($dompet->result() as $row):?>
        <form action="<?= base_url('keuangan/dompet/ubah?id='.$row->id);?>" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" value="<?= $row->nama;?>" class="form-control">
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-success btn-lg">Simpan</button>
            </div>
        </form>
    <?php endforeach;?>
<?php else:?>
    <center>Data tidak ditemukan</center>
<?php endif;?>