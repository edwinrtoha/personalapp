<form action="<?= base_url('keuangan/dompet');?>" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Saldo Awal</label>
                <input type="number" name="saldo" class="form-control">
            </div>
        </div>
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-success btn-lg">Simpan</button>
    </div>
</form>