<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Akun_model extends CI_Model{
        var $CI = NULL;
        public function __construct() {
            $this->CI =& get_instance();
        }
        function login($data=''){
            if(!empty($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('username','password');
                        foreach($allowed as $param){
                            if($param=='password'){
                                $this->db->where($param,md5($data['param'][$param]));
                            }
                            else{
                                if(isset($data['param'][$param])){
                                    $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                                }
                            }
                        }
                    }
                }
            }

            $this->db->select('id');
            $this->db->limit(1);
            $query=$this->db->get('user');
            if($query->num_rows()){
                unset($data['password']);
                $data=array_merge($query->result_array()[0],$data);
                $this->session->set_userdata('login',$data);
                return 1;
            }
            else{
                return 0;
            }
        }
        function login_api($data='',&$response=NULL){
            $this->db->trans_begin();
            if(!empty($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('username','password');
                        foreach($allowed as $param){
                            if($param=='password'){
                                $this->db->where($param,md5($data['param'][$param]));
                            }
                            else{
                                if(isset($data['param'][$param])){
                                    $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                                }
                            }
                        }
                    }
                }
            }

            $this->db->select('id');
            $this->db->limit(1);
            $query=$this->db->get('user');
            if($query->num_rows()){
                unset($data['password']);
                $id_user=$query->result_array()[0]['id'];
                $data=array(
                    'id_user'=>$id_user,
                    'ip'=>$_SERVER['REMOTE_ADDR'],
                    'expired'=>'86400'
                );
                $data['token']=md5($data['id_user'].'-'.$data['ip'].'-'.date('Y-m-d H:i:s'));
                if($this->db->insert('session_token',$data)){
                    if($this->db->trans_status()===TRUE){
                        $this->db->trans_commit();
                        $response=array('token'=>$data['token']);
                        return 1;
                    }
                    else{
                        $response=array();
                        $this->db->trans_rollback();
                        return 0;
                    }
                }
                else{
                    $response=array();
                    return 0;
                }
            }
            else{
                $response=array();
                return 0;
            }
        }
        function validate_token($token,&$response=NULL){
            $this->db->where('token',$this->db->escape_str($token));
            if($query=$this->db->get('session_token')){
                if($query->num_rows()){
                    $response=array('success');
                    return 1;
                }
                else{
                    $response=array();
                    return 0;
                }
            }
            else{
                $response=array();
                return 0;
            }
        }
        function logout(){
            session_destroy();
        }
        function ceklogin(){
            if($this->session->userdata('login')){
                return 1;
            }
            else{
                return 0;
            }
        }
    }
?>