-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Feb 2020 pada 12.34
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `keuangan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `personalapp_dompet`
--

CREATE TABLE `personalapp_dompet` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `saldo` double(13,3) NOT NULL DEFAULT 0.000
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `personalapp_dompet`
--

INSERT INTO `personalapp_dompet` (`id`, `nama`, `saldo`) VALUES
(5, 'Dompet', 87000.000),
(6, 'OVO Cash', 251505.000),
(7, 'OVO Point', 1090.000),
(8, 'Dana', 251.000),
(9, 'Mandiri', 377161.000),
(10, 'Jenius (M-Card)', 73820.000),
(11, 'Jenius (Flexi Saver)', 7847491.000),
(12, 'Jenius (E-Card)', 24300.000),
(13, 'Jenius (X-Card Purple)', 238621.000),
(14, 'Jenius (X-Card Green)', 0.000),
(15, 'LinkAja', 0.000),
(16, 'Cashbac', 1330.000),
(17, 'Gopay', 151999.000),
(18, 'ShoppePay', 358650.000),
(19, 'Koin Shoppe', 52785.000),
(20, 'BCA', 2000.000),
(21, 'BCA Coba', 2000.000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `personalapp_sirkulasi`
--

CREATE TABLE `personalapp_sirkulasi` (
  `id` int(11) NOT NULL,
  `id_dompet` int(11) NOT NULL,
  `keterangan` longtext DEFAULT NULL,
  `jenis` enum('masuk','keluar') NOT NULL,
  `nominal` decimal(13,3) NOT NULL,
  `timestamp_sirkulasi` timestamp NOT NULL DEFAULT current_timestamp(),
  `timestamp_input` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `personalapp_sirkulasi`
--

INSERT INTO `personalapp_sirkulasi` (`id`, `id_dompet`, `keterangan`, `jenis`, `nominal`, `timestamp_sirkulasi`, `timestamp_input`) VALUES
(10, 6, 'Tokopedia - XL XTRA COMBO VIP 10GB+10GB 085862277935 (ibu) (split)', 'keluar', '-78428.000', '2020-02-12 04:57:00', '2020-02-12 23:38:56'),
(11, 7, 'Tokopedia - XL XTRA COMBO VIP 10GB+10GB 085862277935 (ibu) (split)', 'keluar', '-17072.000', '2020-02-12 04:57:00', '2020-02-12 23:41:13'),
(12, 5, 'Tokopedia - XL *PROMO Anynet & XTRA Combo VIP 10GB+10GB 087834795536 (edwin)', 'keluar', '-109000.000', '2020-02-12 05:12:00', '2020-02-12 23:43:18'),
(13, 7, 'Cashback Tokopedia - XL *PROMO Anynet & XTRA Combo VIP 10GB+10GB 087834795536 (edwin)', 'masuk', '1090.000', '2020-02-12 05:12:00', '2020-02-12 23:43:55'),
(14, 5, 'Uang di laci komputer', 'masuk', '59000.000', '2020-02-13 08:10:00', '2020-02-13 02:35:32'),
(15, 5, 'Jajan di citramart', 'keluar', '-22000.000', '2020-02-13 07:10:00', '2020-02-13 07:38:01');

--
-- Trigger `personalapp_sirkulasi`
--
DELIMITER $$
CREATE TRIGGER `after_delete_sirkulasi` AFTER DELETE ON `personalapp_sirkulasi` FOR EACH ROW UPDATE personalapp_dompet as dompet
JOIN personalapp_dompet as dompet_old ON dompet_old.id=OLD.id_dompet
SET dompet.saldo=dompet_old.saldo-(OLD.nominal)
WHERE dompet.id=OLD.id_dompet
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_insert_sirkulasi` AFTER INSERT ON `personalapp_sirkulasi` FOR EACH ROW UPDATE personalapp_dompet as dompet
JOIN personalapp_dompet as dompet_old ON dompet_old.id=NEW.id_dompet
SET dompet.saldo=dompet_old.saldo+(NEW.nominal)
WHERE dompet.id=NEW.id_dompet
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personalapp_user`
--

CREATE TABLE `personalapp_user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `personalapp_user`
--

INSERT INTO `personalapp_user` (`id`, `username`, `password`) VALUES
(1, 'edwinrtoha', '0f6627954ecafdaf1567617a4f301cb6');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `personalapp_dompet`
--
ALTER TABLE `personalapp_dompet`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `personalapp_sirkulasi`
--
ALTER TABLE `personalapp_sirkulasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sirkulasi_ibfk_1` (`id_dompet`);

--
-- Indeks untuk tabel `personalapp_user`
--
ALTER TABLE `personalapp_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `personalapp_dompet`
--
ALTER TABLE `personalapp_dompet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `personalapp_sirkulasi`
--
ALTER TABLE `personalapp_sirkulasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `personalapp_user`
--
ALTER TABLE `personalapp_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `personalapp_sirkulasi`
--
ALTER TABLE `personalapp_sirkulasi`
  ADD CONSTRAINT `personalapp_sirkulasi_ibfk_1` FOREIGN KEY (`id_dompet`) REFERENCES `personalapp_dompet` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
